# LOG - Sprint Backlog [WIP]

Projeto Para materia de PAVI 2017.2.

- Login
- Registro
- Validação de Session
- Escrever e Deletar Cookie
- Pagina Padrão 404 (Configurado no web.xml)
- Logout
- Painel [WIP]

## Tecnologias

- Java EE
- Servlet
- Sqlite
- JSP
- Session
- Cookie
- BootStrap

## Requisitos

- JDK 8
- Tomcat 9

## Preparação

- Adicione o endereço do bd, que se encontra na pasta src/com/logg/resource, na variavel path em ConnectionFactory.java

```sh
$ String path = "E:/projeto-pavi/logg/src/com/logg/resources/data.sqlite3";
```

- Adicione os arquivos .jar na pasta src/com/logg/lib no Java Build Path

| Libs |
|------|
|javax.faces-2.2.8.jar|
|javax.servlet.jar|
|sqlite-jdbc-3.21.0.jar|
