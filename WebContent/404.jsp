<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">

		<title>404 | Pagina n�o encontrada</title>

		<link rel="stylesheet" href="./assets/libs/bootstrap/v4/bootstrap.min.css">
		<link rel="stylesheet" href="./assets/css/404.css">
	</head>

	<body class="conteudo">
		<div class="msg">
			<div class="error-code">404</div>
			<div class="error-message">P�gina n�o encontrada</div>
			<div class="button-place">
            	<a href="index.jsp" class="btn btn-default btn-lg btn-custom">Home</a>
        	</div>
		</div>

		<script src="assets/js/jquery-2.1.4.min.js"></script>
		<script src="assets/js/popper.min.js"></script>
		<script src="assets/libs/bootstrap/v4/bootstrap.min.js"></script>
	</body>
</html>