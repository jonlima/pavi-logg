<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">

		<title>Logg - Sprint Backlog</title>

		<link rel="stylesheet" href="assets/libs/bootstrap/v4/bootstrap.min.css">
		<link rel="stylesheet" href="assets/css/login.css">
	</head>

	<body class="text-center">
		<form class="form-signin" action="login" method="post">
			<h1 class="h3 mb-3 font-weight-normal">Logg - Login</h1>
			
			<label for="username" class="sr-only">Usuario</label>
      		<input type="text" id="username" name="username" class="form-control" placeholder="Usuario" required autofocus>

      		<label for="password" class="sr-only">Senha</label>
      		<input type="password" id="password" name="password" class="form-control" placeholder="Senha" required>

      		<input type="submit" value="Login" class="btn btn-lg btn-primary btn-block">

      		<div class="register mb-3">
      			<label>N�o tem uma conta ? <a href="/logg/register" class="waves-effect">Cadastre-se</a></label>
      		</div>
		</form>

		<script src="assets/js/jquery-2.1.4.min.js"></script>
		<script src="assets/js/popper.min.js"></script>
		<script src="assets/libs/bootstrap/v4/bootstrap.min.js"></script>
	</body>
</html>