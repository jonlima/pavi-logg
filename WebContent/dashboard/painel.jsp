<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%
	if(session.getAttribute("user") == null) {
		response.sendRedirect("/logg/");
	} else {
%>
	
	<!DOCTYPE html>
	<html>
		<head>
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
			<title>Painel</title>
			
			<link rel="stylesheet" href="../assets/libs/bootstrap/v4/bootstrap.min.css">
			<link rel="stylesheet" href="../assets/css/style.css">
		</head>
		
		<body>
		
			<nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0">
				<a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">Logg - Sprint Backlog</a>
				<ul class="navbar-nav px-3">
					<li class="nav-item text-nowrap">					
						<form action="logout" method="post" class="nav-link">
							<input type="submit" class="btn btn-outline-info" value="Sair">
						</form>
					</li>
				</ul>
			</nav>
			
			<div class="container-fluid">
				<h1>Projetos</h1>
			</div>
		</body>
	</html>

<%
	}
%>