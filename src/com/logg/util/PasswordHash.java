package com.logg.util;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class PasswordHash {
	public static String hashPass(String pw) {
		String hw = null;
		
		try {
			MessageDigest md5 = MessageDigest.getInstance("MD5");
			md5.update(pw.getBytes());
			BigInteger hash = new BigInteger(1, md5.digest());
			hw = hash.toString(16);
		} catch (NoSuchAlgorithmException e) {
			System.out.println("Erro ao gerar hash: " + e.getMessage());
		}
		
		return hw;
	}
	
	public boolean checkPassword(String pw) {
		String hashedPass = hashPass(pw);
		
		if(hashedPass.equals("")) {
			return true;
		}
		
		return false;
	}
}
