package com.logg.resources;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.*;
import org.sqlite.JDBC;

import com.logg.util.PasswordHash;
import com.logg.model.User;

import com.logg.resources.*;

public class ConnectionFactory {	
	public static Connection getConnection()  {
		Connection conn = null;
		
		try {
			String path = "E:/projeto-pavi/logg/src/com/logg/resources/data.sqlite3";
			Class.forName("org.sqlite.JDBC");
			conn = DriverManager.getConnection("jdbc:sqlite:"+path);
			System.out.println("Conectado...");
		} catch (ClassNotFoundException | SQLException e) {
			System.out.println(e);
			System.out.println(e.getMessage());
		} 
		
		return conn;
	}
	
	public static void closeConnection(Connection conn) {
		if(conn != null) {
			try {
				conn.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * 
	 * Usado para testar se conecta com o banco de dad
	 * Basta rodar esse arquivo em RUN AS -> JAVA APPLICATION
	 * 
	 * */
	
	/*
	public static void main(String[] args) throws SQLException {
		Connection c = getConnection();		
		PreparedStatement ps = null;
		ResultSet rs = null;
		ps = c.prepareStatement("SELECT * FROM users");
		rs = ps.executeQuery();
		
		while(rs.next()) {
			String username = rs.getString("username");
			System.out.println(username);
		}
		
		closeConnection(c);
	}
	*/
}
