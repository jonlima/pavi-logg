package com.logg.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.logg.util.*;
import com.logg.resources.*;

public class RegisterServlet extends HttpServlet {
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		String username = req.getParameter("username");
		String password = req.getParameter("password");
		
		String sql = "INSERT INTO users (username, password) VALUES (?, ?)";
		Connection conn = ConnectionFactory.getConnection();
		
		try {
			PreparedStatement ps = null;
			ps = conn.prepareStatement(sql);
			ps.setString(1, username.toLowerCase());
			ps.setString(2, PasswordHash.hashPass(password));
			ps.execute();
			ConnectionFactory.closeConnection(conn);
			
			PrintWriter out = res.getWriter();
			out.println("<script type=\"text/javascript\">");  
			out.println("alert('Registro realizado com sucesso.');");  
			out.println("</script>");

			res.sendRedirect("/logg/");
		} catch (Exception e) {
			System.out.println(e);
		}
	}
}
