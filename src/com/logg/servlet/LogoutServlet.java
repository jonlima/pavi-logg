package com.logg.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class LogoutServlet extends HttpServlet {
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		HttpSession session = req.getSession();
		session.invalidate();
		
		Cookie cookie = new Cookie("username", null);		
		cookie.setPath("/logg");
		cookie.setMaxAge(0);
		
		res.addCookie(cookie);
		res.sendRedirect("/logg/");
	}
}
