package com.logg.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.logg.util.*;
import com.logg.resources.*;

import com.logg.model.User;

public class LoginServlet extends HttpServlet {
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		String username = req.getParameter("username");
		String password = req.getParameter("password");
		
		String sql = "SELECT * FROM users WHERE username = ? AND password = ?";
		Connection conn = ConnectionFactory.getConnection();
		
		try {
			PreparedStatement ps = null;
			ResultSet rs = null;
			User user = null;
			
			ps = conn.prepareStatement(sql);
			ps.setString(1, username.toLowerCase());
			ps.setString(2, PasswordHash.hashPass(password));
			rs = ps.executeQuery();
			
			if(rs.next()) {
				user = new User();
				user.setCod(rs.getInt("cod"));
				user.setUsername(rs.getString("username"));
				
				
				Cookie ck = new Cookie("username", user.getUsername());
				ck.setMaxAge(30*60);
				res.addCookie(ck);
				req.getSession().setAttribute("user", user);
				res.sendRedirect("/logg/dashboard/painel.jsp");
			}			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
